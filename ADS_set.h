//
// Created by Violeta Petkova on 5/15/2018.
//

#ifndef ADS_SET_H
#define ADS_SET_H

#include <functional>
#include <algorithm>
#include <iostream>
#include <stdexcept>
#include <unordered_map>
#include <cmath>


using namespace std;


template <typename Key, size_t N=7>
class ADS_set {
public:
    class Iterator;

    using value_type = Key;
    using key_type = Key;
    using reference = key_type &;
    using const_reference = const key_type &;
    using size_type = size_t;
    using difference_type = std::ptrdiff_t;
    using iterator = Iterator;
    using const_iterator = Iterator;
    using key_compare = std::less<key_type>;   // B+-Tree
    using key_equal = std::equal_to<key_type>; // Hashing
    using hasher = std::hash<key_type>;        // Hashing

private:
    struct element {
        key_type key;
        size_t idx; //element index
        element *next = nullptr;
    };

    element **table{nullptr}; //hash table

    size_type curr_size{0};   // number of elements in the table
    size_type table_size{N}; //size of table
    float max_factor{0.7}; //max load factor

    size_type hash_idx(const key_type &key) const { return hasher{}(key) % table_size; }

    element *insert_unchecked(const key_type &key); //insert new element to the table

    void resize(); // make a new table

public:
    ADS_set() {
        table = new element *[N + 1];
        for (size_type i{0}; i < N + 1; ++i) { table[i] = nullptr; }
    } //new empty Container
    ADS_set(std::initializer_list<key_type> ilist) : ADS_set{} { insert(ilist); }

    template<typename InputIt>
    ADS_set(InputIt first, InputIt last): ADS_set{} { insert(first, last); }

    ADS_set(const ADS_set &other) : ADS_set() { for (const auto &key: other) insert(key); } //Copy Constuctor
    ~ADS_set() {
        for (size_type i{0}; i < table_size + 1; ++i) {
            while (table[i] != nullptr) {
                element *delete_old = table[i];
                table[i] = table[i]->next;
                delete delete_old;
            }
        }
        delete[] table;
    }

    ADS_set &operator=(const ADS_set &other) {
        if (this == &other) return *this;
        clear();
        insert(other.begin(), other.end());
        return *this;
    }

    ADS_set &operator=(std::initializer_list<key_type> ilist) {
        clear();
        insert(ilist);
        return *this;
    }

    size_type size() const { return curr_size; }

    bool empty() const { return !curr_size; }

    size_type count(const key_type &key) const;

    iterator find(const key_type &key) const {  // same as count but it returns an iterator
        size_type idx{hash_idx(key)};
        element *temp = table[idx];

        if (table[idx] != nullptr) {
            while (temp != nullptr) {
                if (key_equal{}(temp->key, key)) { return const_iterator(temp, table, table_size); }
                else
                    temp = temp->next;
            }
            temp = nullptr;
            delete temp;
        }
        temp = nullptr;
        delete temp;
        return end();
    }


    void clear() {
        for (size_type i{0}; i < table_size + 1; ++i) {
            while (table[i] != nullptr) {
                element *delete_old = table[i];
                table[i] = table[i]->next;
                delete delete_old;
            }
        }
        curr_size = 0;
    }

    void swap(ADS_set &other) {
        using std::swap;
        swap(table, other.table);
        swap(curr_size, other.curr_size);
        swap(table_size, other.table_size);
        swap(max_factor, other.max_factor);
    }

    void insert(std::initializer_list<key_type> ilist);

    std::pair<iterator, bool> insert(const key_type &key) {
        if (!count(key)) {

            if (curr_size / (double) table_size >= max_factor) {
                resize();

            }
            size_type idx{hash_idx(key)};
            element *temp = table[idx];
            element *new_elem = new element;
            new_elem->key = key;


            if (temp != nullptr) {
                new_elem->idx = idx;
                table[idx] = new_elem;
                new_elem->next = temp;
            } else {
                new_elem->idx = idx;
                table[idx] = new_elem;
            }
            ++curr_size;
            temp = nullptr;
            delete temp;
            return std::make_pair(const_iterator{table[idx], table, table_size}, true);
        } else {
            const_iterator temp = find(key);
            return std::make_pair(const_iterator{temp}, false);
        }
    }

    template<typename InputIt>
    void insert(InputIt first, InputIt last);

    size_type erase(const key_type &key) {
        if (count(key)) {
            size_type idx{hash_idx(key)};
            element *temp = table[idx];
            element *beforetemp = nullptr;

            if (table[idx] != nullptr) {
                if (key_equal{}(temp->key, key)) {
                    table[idx] = table[idx]->next;
                    delete temp;
                    --curr_size;
                    return 1;
                } else {
                    while (temp->next) {
                        beforetemp = temp->next;
                        if (key_equal{}(beforetemp->key, key)) {
                            temp->next = beforetemp->next;
                            beforetemp->next = nullptr;
                            delete beforetemp;
                            --curr_size;
                            return 1;
                        }

                        temp = temp->next;
                    }
                }
            }

        }

        return 0;
    }

    const_iterator begin() const {
        for (size_type i{0}; i < table_size; ++i) {
            if (table[i] != nullptr) {
                return const_iterator(table[i], table, table_size);
            }

        }
        return end();
    }

    const_iterator end() const { return const_iterator(table[table_size], table, table_size); }

    void dump(std::ostream &o = std::cerr) const;

    friend bool operator==(const ADS_set &lhs, const ADS_set &rhs) {
        if (lhs.curr_size != rhs.curr_size) return false;
        for (const auto &key: rhs) {
            if (!lhs.count(key)) return false;
        }
        return true;
    }

    friend bool operator!=(const ADS_set &lhs, const ADS_set &rhs) { return !(lhs == rhs); }
    };


template <typename Key, size_t N>
class ADS_set<Key,N>::Iterator {
private:
    element *temp;
    element **table;
    size_type table_size;

public:
    using value_type = Key;
    using difference_type = std::ptrdiff_t;
    using reference = const value_type &;
    using pointer = const value_type *;
    using iterator_category = std::forward_iterator_tag;

    explicit Iterator(element *temp, element **table, size_type table_size) : temp(temp), table(table), table_size(table_size) {}
    Iterator() {}

    reference operator*() const { return temp->key; }

    pointer operator->() const { return &temp->key; }

    Iterator& operator++() {
        if (temp != nullptr && temp->next!= nullptr) {
            temp = temp->next;
            return *this;
        }
        else
        {
            size_type hash_idx = 0;
            hash_idx = temp->idx;
            while(hash_idx <= table_size && table[hash_idx+1] == nullptr) hash_idx++;

            if(hash_idx >= table_size) {
                temp = table[table_size];
            }

            else { //next
                if(temp->next != nullptr) temp = temp->next;
                hash_idx++;
                temp = table[hash_idx];
            }
            return *this;
        }
    }

    Iterator operator++(int) {Iterator rc{*this}; ++*this; return rc;}
    friend bool operator==(const Iterator &lhs, const Iterator &rhs) {return lhs.temp == rhs.temp;}
    friend bool operator!=(const Iterator &lhs, const Iterator &rhs) {return lhs.temp != rhs.temp;}
};

template <typename Key, size_t N> void swap(ADS_set<Key,N>& lhs, ADS_set<Key,N>& rhs) {lhs.swap(rhs); }

template <typename Key,size_t N>
typename ADS_set<Key,N>::element *ADS_set<Key,N>::insert_unchecked(const key_type &key) {
    //check load faktor, if not resize
    if(curr_size/(double)table_size >= max_factor) {
        resize();

    }
    size_type idx{hash_idx(key)};
    element *temp = table[idx];
    element *new_elem = new element;
    new_elem->key = key;


    if(temp!=nullptr) {
        new_elem->idx = idx;
        table[idx] = new_elem;
        new_elem->next=temp;
    }

    else {
        new_elem->idx = idx;
        table[idx] = new_elem;
    }
    ++curr_size;
    temp=nullptr;
    delete temp;
    return *(table+idx);
}

template <typename Key, size_t N>
typename ADS_set<Key,N>::size_type ADS_set<Key,N>::count(const key_type& key) const {
    size_type idx{hash_idx(key)};
    element *temp = table[idx];

    if(table[idx] != nullptr) {
        while(temp!= nullptr) {
            if(key_equal{}(temp->key,key))
                return 1;
            else
                temp = temp->next;
        }
    }
    return 0;
}

template <typename Key, size_t N>
void ADS_set<Key,N>::resize() {

    element **old_table{table};
    size_type old_table_size{table_size};
    table_size = table_size * 2 + 1;
    table = new element *[table_size+1];


    for(size_type i{0}; i < table_size+1; ++i) {
        table[i] = nullptr;
    }

    for (size_type i{0}; i < old_table_size+1; ++i) {
        while(old_table[i]!=nullptr) {
            element *temp = old_table[i];
            size_type idx = hash_idx(temp->key);
            temp->idx = idx;

            if(table[idx]!= nullptr) {
                element *new_elem = table[idx];
                old_table[i] = old_table[i]->next;
                table[idx] = temp;
                temp->next = new_elem;
            }
            else {
                old_table[i] = old_table[i]->next;
                table[idx] = temp;
                table[idx]->next = nullptr;
            }
        }
    }

    //Delete the old table
    for(size_type i {0}; i < old_table_size+1; ++i) {
        while(old_table[i] != nullptr) {
            element *delete_old = old_table[i];
            old_table[i] = old_table[i]->next;
            delete delete_old;
        }
      }
    delete [] old_table;
}

template<typename Key, size_t N>
void ADS_set<Key,N>::insert(std::initializer_list<key_type> ilist) {
    for(const auto &key:ilist) {
        if(!count(key)) insert_unchecked(key);
    }
}

template<typename Key, size_t N>
template<typename InputIt> void ADS_set<Key,N>::insert(InputIt first, InputIt last) {
    for(auto it = first; it !=last; ++it) {
        if(!count(*it)) {
            insert_unchecked(*it);
        }
    }
}

template<typename Key, size_t N>
void ADS_set<Key,N>::dump(std::ostream& o) const {
    for(size_type i{0}; i < table_size+1; ++i) {
        element *temp = table[i];

        if(temp!=nullptr){
            o << i << ": ";
            while(temp!=nullptr){
                o << "->"<<temp->key;
                temp = temp->next;
            }
        }
        else {
            o << i << "-";
        }
        o << '\n';
    }
}
#endif // ADS_SET_H